<?php

namespace Drupal\unwanted_email_registration\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class UnwantedEmailRegistrationConfigForm settings.
 */
class UnwantedEmailRegistrationConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'unwanted_email_registration_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['unwanted_email_registration.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('unwanted_email_registration.settings');
    $form['unwanted_email_registration_settings'] = [
      '#type' => 'vertical_tabs',
    ];

    $form['unwanted_email_registration_settings']['settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Settings'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#group' => 'unwanted_email_registration_settings',
      '#weight' => 0,
    ];

    $form['unwanted_email_registration_settings']['settings']['filter_chars'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Filter Chars'),
      '#default_value' => $config->get('filter_chars'),
      '#required' => TRUE,
      '#description' => $this->t('Characters to prevent email registration with. Separated by comma.'),
    ];

    $form['unwanted_email_registration_settings']['settings']['email_domains'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Domains'),
      '#default_value' => $config->get('email_domains'),
      '#required' => TRUE,
      '#description' => $this->t('Enter domain names of email addresses to apply this rule. One domain on each line.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $config = $this->config('unwanted_email_registration.settings');
    $config->set('filter_chars', $form_state->getValue('filter_chars'))->save();
    $config->set('email_domains', $form_state->getValue('email_domains'))->save();

  }

}
