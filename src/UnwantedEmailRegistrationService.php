<?php

namespace Drupal\unwanted_email_registration;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;

/**
 * Class Unwanted Email Registration.
 */
class UnwantedEmailRegistrationService {

  /**
   * Constructs a new Unwanted Email Registration Service.
   *
   * @param Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   A config factory for retrieving required config objects.
   * @param Drupal\Core\Database\Connection $connection
   *   The current active database's master connection.
   */
  public function __construct(ConfigFactoryInterface $config_factory, Connection $connection) {
    $this->config = $config_factory->get('unwanted_email_registration.settings');
    $this->connection = $connection;
  }

  /**
   * Email Verification.
   *
   * @param string $email
   *   Email.
   */
  public function validateEmail($email) {
    if (!empty($email)) {
      /* If the current and new email address are the same then we can skip this check List of allowed domains. */
      $allowed = array_map('trim', explode("\n", $this->config->get('email_domains')));

      $parts = explode('@', $email);

      $domain = array_pop($parts);

      /* Check if the domain is in our list. */
      if (!in_array($domain, $allowed)) {
        // Not allowed.
        return TRUE;
      }
      /* Check the Email is exists. */
      if (user_load_by_mail(_unwanted_email_registration_filter_email($email))) {
        return FALSE;
      }

      /* Get all system emails. Do this in memory to improve speed and lessen the hit To the database . */
      $query = $this->connection->select('users_field_data', 'u')
        ->fields('u', ['mail']);
      $results = $query->execute()->fetchAll();

      foreach ($results as $result) {
        $filter_email = _unwanted_email_registration_filter_email($result->mail);
        if ($filter_email == $email || $filter_email == _unwanted_email_registration_filter_email($email)) {

          return FALSE;

        }
      }

    }
    return TRUE;
  }

}
