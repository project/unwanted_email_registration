CONTENTS OF THIS FILE
---------------------

 * INTRODUCTION
 * Requirements
 * Installation
 * Configuration
 * Maintainers

-- INTRODUCTION --

* The purpose of this module is to provide for duplicate 
 email addresses checking for domains 
 that allow extraneous characters to 
 be placed inside usernames and be 
 treated as different email addresses. For example:

- abdulaziz@gmail.com
- a.bdulaziz@gmail.com
- ab.dulaziz@gmail.com


-- REQUIREMENTS --

* This module requires no modules outside of Drupal core.


-- INSTALLATION --
* Install as usual, see:
https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
for further information.

 
-- CONFIGURATION --
* Go to configuration » people » Unwanted Email Registration settings.


-- Maintainers --
Current maintainers:
* Abdulaziz zaid (abdulaziz1zaid) - https://www.drupal.org/u/abdulaziz1zaid
