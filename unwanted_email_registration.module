<?php

/**
 * @file
 * unwanted_email_registration.module
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function unwanted_email_registration_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.unwanted_email_registration':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('An extra check of email addresses provided at account creation to check for duplicates using dots or other spacers.') . '</p>';
      return $output;
  }
}

/**
 * Implements hook_form_alter().
 */
function unwanted_email_registration_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  /* If we're usign the user_profile or user_registration form, then validate the email that we are providing. */
  if ($form_id == 'user_register_form') {
    $form['#validate'][] = '_unwanted_email_registration_form_validate';
  }
}

/**
 * Our validation form.
 *
 * @return bool
 *   Retuns false if validation fails
 */
function _unwanted_email_registration_form_validate($form, FormStateInterface $form_state) {
  $user_mail = $form_state->getValue('mail');
  if (!empty($user_mail)) {
    $service = \Drupal::service('unwanted_email_registration.services');
    if ($service->validateEmail($user_mail) === FALSE) {
      $form_state->setErrorByName('mail', t('This email address is already in use by another user. Please select another one.'));
      return FALSE;
    }
  }

}

/**
 * Function to filter out unwanted comparison characters.
 *
 * @param string $email
 *   The unfiltered email address we wish to sanitize.
 */
function _unwanted_email_registration_filter_email($email) {
  $config = \Drupal::config('unwanted_email_registration.settings');
  $filter_chars = explode(',', $config->get('filter_chars'));
  $explode = explode('@', $email);
  $email = str_replace($filter_chars, "", $explode[0]);
  return $email . "@" . $explode[1];
}
